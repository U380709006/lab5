import java.util.Scanner;
public class GCDRec {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter first number");
        int num1 = input.nextInt();
        System.out.println("Enter second number");
        int num2 = input.nextInt();
        System.out.println("The greatest common factor of " + num1 + " " + num2 + " is " + gcd(num1,num2));
    }

    private static int gcd(int num1, int num2){
        if (num2 == 0) {
            return num1;
        }
        return gcd(num2, num1 % num2);
    }
}
